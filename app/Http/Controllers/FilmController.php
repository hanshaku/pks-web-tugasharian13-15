<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;

class FilmController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except([]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Genre $genre)
    {
        $films = $genre->films;
        return view('film.index', compact('films', 'genre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Genre $genre)
    {
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Genre $genre)
    {
        $request->validate([
            'Judul' => "required",
            'Ringkasan' => "required",
            'Tahun' => "required",
            'Poster' => "required"
        ]);
        Film::create([
            'Judul' => $request->Judul,
            'Ringkasan' => $request->Ringkasan,
            'Tahun' => $request->Tahun,
            'Poster' => $request->Poster,
            'genre_id' => $genre->id
        ]);
        return redirect()->route('film.index', compact('genre'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Genre $genre, Film $film)
    {
        // $genre = Genre::find($id);
        // return view(Genre $genre, Film $film);
        return view('genre.show', compact('genre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Genre $genre, Film $film)
    {
        // $genre = Genre::find($id);
        return view('genre.edit', compact('genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Genre $genre, Film $film)
    {
        $request->validate([
            'Judul' => "required",
            'Ringkasan' => "required",
            'Tahun' => "required",
            'Poster' => "required"
        ]);

        $genre->nama = $request->nama;
        $genre->update();
        return redirect('/genre');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Genre $genre, Film $film)
    {
        $genre->delete();
        return redirect('/genre');
    }
}
