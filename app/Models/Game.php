<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $tabel  = 'games';
    protected $guarded = ["id"];
    public $timestamps = false;
}
