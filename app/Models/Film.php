<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = "film";
    protected $fillable = ['Judul', "Ringkasan", "Tahun", "Poster", "genre_id"];

    public function ratingkomentars()
    {
        return $this->hasMany(Ratingkomentar::class);
    }

    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }
}
