<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = "profil";
    protected $fillable = [
        'umur',
        'Bio',
        'Alamat'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
