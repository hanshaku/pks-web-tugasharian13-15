<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\ProfilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__ . '/auth.php';

Route::get('/', [IndexController::class, 'index']);

Route::get('/Register', [AuthController::class, 'index']);

Route::post('/Welcomes', [AuthController::class, 'store']);

Route::get('/master', function () {
    return view('layouts.master');
});
Route::get('/data-table', function () {
    return view('data-table');
});

Route::get('/genre', [GenreController::class, 'index'])->name('genre.index');

Route::group(
    [
        'prefix' => 'genre',
        'name' => 'genre.'
    ],
    function () {
        Route::get('/', [GenreController::class, 'index'])->name('index');
        Route::get('/create', [GenreController::class, 'create'])->name('create');
        Route::post('/', [GenreController::class, 'store'])->name('store');
        Route::get('/{genre}', [GenreController::class, 'show'])->name('show');
        Route::get('/{genre}/edit', [GenreController::class, 'edit'])->name('edit');
        Route::put('/{genre}', [GenreController::class, 'update'])->name('update');
        Route::delete('/{genre}', [GenreController::class, 'destroy'])->name('destroy');
    }
);

Route::group(
    [
        'prefix' => 'film',
        'name' => 'film.'
    ],
    function () {
        Route::get('/{genre}', [FilmController::class, 'index'])->name('film.index');
        Route::get('/{genre}/create', [FilmController::class, 'create'])->name('create');
        Route::post('/{genre}', [FilmController::class, 'store'])->name('store');
        Route::get('/{genre}/{film}', [FilmController::class, 'show'])->name('show');
        Route::get('/{genre}/{film}/edit', [FilmController::class, 'edit'])->name('edit');
        Route::put('/{genre}/{film}', [FilmController::class, 'update'])->name('update');
        Route::delete('/{genre}/{film}', [FilmController::class, 'destroy'])->name('destroy');
    }
);



Route::resource('profil', ProfilController::class)->only([
    'index', 'update'
]);


Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');


Route::get('/post', [PostController::class, 'index'])->name('post.index');
Route::get('/post/create', [PostController::class, 'create'])->name('post.create');
Route::post('/post', [PostController::class, 'store'])->name('post.store');
Route::get('/post/{id}', [PostController::class, 'show'])->name('post.show');
Route::get('/post/{id}/edit', [PostController::class, 'edit'])->name('post.edit');
Route::put('/post/{id}', [PostController::class, 'update'])->name('post.update');
Route::delete('/post/{id}', [PostController::class, 'destroy'])->name('post.destroy');

Route::get('/cast', [CastController::class, 'index'])->name('cast.index');
Route::get('/cast/create', [CastController::class, 'create'])->name('cast.create');
Route::post('/cast', [CastController::class, 'store'])->name('cast.store');
Route::get('/cast/{id}', [CastController::class, 'show'])->name('cast.show');
Route::get('/cast/{id}/edit', [CastController::class, 'edit'])->name('cast.edit');
Route::put('/cast/{id}', [CastController::class, 'update'])->name('cast.update');
Route::delete('/cast/{id}', [CastController::class, 'destroy'])->name('cast.destroy');



Route::get('/game', [GameController::class, 'index'])->name('game.index');
Route::get('/game/create', [GameController::class, 'create'])->name('game.create');
Route::post('/game', [GameController::class, 'store'])->name('game.store');
Route::get('/game/{id}', [GameController::class, 'show'])->name('game.show');
Route::get('/game/{id}/edit', [GameController::class, 'edit'])->name('game.edit');
Route::put('/game/{id}', [GameController::class, 'update'])->name('game.update');
Route::delete('/game/{id}', [GameController::class, 'destroy'])->name('game.destroy');
