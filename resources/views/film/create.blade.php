@extends('layouts.master')
@section('title','Create genre')
@section('content')
<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Create Data</title>

</head>

<body>

<h2>Create Data genre</h2>
<form action="/film/{{$genre->id}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="Judul">Film</label>
                <input type="text" class="form-control" name="Judul" id="Judul" placeholder="Masukkan Nama judul film">
                @error('Judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="Ringkasan">Ringkasan</label>
                <input type="text" class="form-control" name="Ringkasan" id="Ringkasan" placeholder="Masukkan Ringkasan film">
                @error('Ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="Tahun">Tahun</label>
                <input type="text" class="form-control" name="Tahun" id="Tahun" placeholder="Masukkan Tahun film">
                @error('Tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="Poster">Poster</label>
                <input type="text" class="form-control" name="Poster" id="Poster" placeholder="Masukkan Poster film">
                @error('Poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html> 
        
@endsection