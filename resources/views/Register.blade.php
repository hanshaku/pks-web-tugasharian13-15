@extends('layouts.master')
@section('tittle', "Selamat Mendaftar!")
@section('judulHal', "Selamat Mendaftar!!")

@section('content')
<div class="p-3">
    <h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>
    <form action="Welcomes" method="post">
        @csrf
        <label for="firstName">First Name :</label><br>
        <input type="text" name="FirstName" id="firstName"><br>
        <label for="lastname">Last Name :</label><br>
        <input type="text" name="LastName" id="lastname"><br>
        <p>Gender</p>
        <input type="radio" name="gender" id="genderMale">Male<br>    
        <input type="radio" name="gender" id="genderFemale">Female <br> <br>
        <label for="nationality">Nationality</label><br>
        <select name="Nationality" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select>
        <p>Language Spoken</p>
        <input type="checkbox" name="LanguageSpoken" id="language">Bahasa Indonesia<br>
        <input type="checkbox" name="LanguageSpoken" id="language">English<br>
        <input type="checkbox" name="LanguageSpoken" id="language">Other<br> <br>
        <label for="bio">Bio</label><br>
        <textarea name="Bio" id="" cols="30" rows="7"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
    </div>
@endsection

{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daftar Baru</title>
</head>
<body>
    
</body>
</html> --}}