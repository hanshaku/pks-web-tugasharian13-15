@extends('layouts.master')
@section('title','List Cast ')
@section('content')

<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Table Data</title>

</head>

<body>

<h2>List Genre</h2>

<a href="/genre/create" class="btn btn-primary mb-2">Tambah</a>

<table class="table">

<thead class="thead-light">

<tr>

<th scope="col">#</th>

<th scope="col">Name</th>

<th scope="col">Films</th>

<th scope="col">Action</th>

</tr>

</thead>

<tbody>

@forelse ($genre as $key=>$genre)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$genre->nama}}</td>
                        <td>
                        @foreach ($genre->films as $index => $film) {{$film->Judul}}
                            
                        @endforeach
                        </td>
                        <td>
                            <a href="/genre/{{$genre->id}}" class="btn btn-info">Show</a>
                            <a href="/genre/{{$genre->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/genre/{{$genre->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                            <a href="/film/{{$genre->id}}" class="btn btn-primary">Film</a>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse  

</tbody>

</table>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>

@endsection